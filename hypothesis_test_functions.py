from statistics import NormalDist # Available in Python 3.8+
from ci_functions import z_score # We're reusing the same z score calculator we made in the other file!

# We need to import a test case and the data set that we're going to test!
def two_tailed_hypothesis_test(test, data):
    dist = NormalDist.from_samples(data) # We create another normal distribution from the data set we're passing in
    z = z_score(1 - test[1]) # The z score calculator is used again, but we need to back out confidence from alpha!
    count = data.count() # Here's the count of data points we have in our list
    t = abs(test[0] - dist.mean) / (dist.stdev / (count ** 0.5)) # The test statistic equation is used here
    # The test statistic automatically creates all of the above from our test data set!
    if t > z: # This is the condition that we reject the null
        return "\tWe reject the null hypothesis!" # We're just going to return if it's 
    else:
        return "\tWe fail to reject the null."