# Import runtime dependencies (packages) to your local machine via pip3, via the terminal. 
import csv
import numpy as np #To install a package, on Mac/Linus (Unix-based systems), Terminal this: $ pip3 install numpy
import pandas as pd # Terminal this: $ pip3 install numpy
from statistics import NormalDist # Available in Python 3.8+

from ci_functions import confidence_interval_exp, confidence_interval_means, z_score
from norm_functions import normal_distribution_probabilities
from hypothesis_test_functions import two_tailed_hypothesis_test

"""

Pip is the package manager for local Python dependencies. Think of runtime dependencies as
browser extensions that serve a specific purpose.

We need to properly access the csv data. To do so, save the downloaded Excel file as a csv.
You should store the CSV in the same file as your program (python) files:

assignment_2
| main.py
| stat_equations.py
| deliveries.csv

"""

# We need to import the csv data into Python, and we're going to use a Pandas data frame to structure data
# We declare the variable df, which will hold the data being imported and easy to access
# pd refers to the pandas package, and we are going to run the pandas method that reads csv files!
# inside the parenthesis is r, for read (pandas can also write data to a csv), and the filename
df = pd.read_csv (r'deliveries.csv')

# If you do not have the csv in the same directory, 
# then you will need an absolute path or change the relative pathing!
# For example:
# pd.read_csv (r'/Users/matthallprd/Dropbox/Simon/Core Statistics/VSCode_Workspace/deliveries.csv')
# Programmers avoid absolute pathing, so the code works on any computer or server!!
# A server can read a file in the directory, but cannot grab it from my laptop. (*Technically could if setup via SSH tunnel)

print("\n\tData Frame:\n")
print(df)
# We should print messages to the terminal occassionally to check our work! Aka logging
# If you don't want to see it anymore, turn it into a comment with a hashtag!

# Let's look at some summary statistics:
print("\n\tSummary Statistics:\n") # This prints text to the terminal, and \n is a line break to look neater
print(df.describe()) # This prints out summary statistics, by running the Pandas .describe() method on the data (df)

# For this data, we really don't care about delivery order. Let's just focus on delivery times!
deliveries = pd.Series(df['DeliveryTime'])
print("\n\tDelivery Time Series:\n")
print(deliveries)

"""
Let's break this one down. deliveries is the variable we're going to store data in.
pd is telling Python to find a Pandas method in the runtime dependencies
Series is a Pandas method that declares a Series of data. A series is essentially a list of data with a header
    In this case the series is just a column of data separated out
df['DeliveryTime'] is declaring that we want the series pulled out of our data frame (df), and to ...
    pull out the column of data, DeliveryTime
"""

##### Homework, Part A:
# Calculate sample mean, sample std, and sample variance
# There are a lot of ways to do this!!

sample_mean = deliveries.mean()
# This is running the Pandas mean method on our data set!
# Remember that deliveries = pd.Series(df['DeliveryTime']) \\ we're really adding the .mean() method on the end!

"""Here's a bunch of ways to run the same thing:
# Uncomment this block to see for yourself! (Delete the triple quotes)

sample_mean_alt1 = pd.Series(df['DeliveryTime']).mean()  #This calculates mean from a series by grabbing a series from the data frame
sample_mean_alt2 = df.loc[:,"DeliveryTime"].mean()  #This calculates mean by isolating a column in the data frame
sample_mean_alt3 = pd.read_csv (r'deliveries.csv').loc[:,"DeliveryTime"].mean()
# alt3 reimports the data. NOT recommended as it impacts performance, but it is possible.

sample_mean_alt4 = np.mean(deliveries) #We can also use a numpy method too! Pass the delivery series into numpy!

print(sample_mean_alt1)
print(sample_mean_alt2)
print(sample_mean_alt3)
print(sample_mean_alt4)
"""

# We can calculate standard deviation and variance in a similar manner
# Pandas defaults to 1 degree of freedom, so no need to declare
sample_var = deliveries.var()
sample_std = deliveries.std()

# Numpy defaults to 0 ddof! Here's the numpy method:
sample_std_numpy = np.std(deliveries, ddof=1)
sample_var_numpy = np.var(deliveries, ddof=1)

# Let's print out our answers to the terminal:
print("\n\n\tPart A:")
print("\nSample Mean: " + str(sample_mean))
print("Sample Var: " + str(sample_var))
print("Sample Std: " + str(sample_std))

##### Part C: We need to create a 95% interval for expected delivery time mean!
# We can do that with the generic Z score estimates, but we can get much more accurate!

# It's now time to introduce functions, or repeatable blocks of code with inputs / outputs!
# I provide a detailed breakdown of the two confidence interval functions in ci_functions.py

# We call the function and save it to a variable below, twice:
expected_interval_95 = confidence_interval_exp(deliveries)  # Confidence default to .95, so not input necessary
expected_interval_80 = confidence_interval_exp(deliveries, .80) # We set confidence manually as second input

print("\n\n\tPart C:")
print("\n95% CI for Expected:")
print(str(expected_interval_95))
print("\n80% CI for Expected:")
print(str(expected_interval_80))

##### Part D, Find Probabilities of Normal Distribution!
# We can do this all in one step by creating pairs with the value and operator, less or more
# This is so we can calculate all three probability calculations in one step!
# Look at norm_functions.py for a breakdown of the function!

probability_pairs = [[45, "less"], [40, "less"], [50, "more"]]
part_d_results = normal_distribution_probabilities(deliveries, probability_pairs)

# The function for part D results is going to return a list
# We're going to use a for loop to iterate through each element, and print out that element!
print("\n\n\tPart D:\n")
for result in part_d_results:
    print(result)

##### Part F, Time to calculate CIs for the mean!

# Back to confidence intervals! Now we're going to call the means method in the ci_functions.py file!
# Since we're not passing in a confidence level, it defaults to 95%
conf_interval_1 = confidence_interval_means(deliveries)
print("\n\n\tPart F:")
print("\n95% CI for Sample Means:")
print(str(conf_interval_1))

##### Part H, Time for Hypothesis Testing!!

# Below are the two null hypotheses to test, with the given mean and alpha level to test at:
null_hypothesis_1 = [50, .05]
null_hypothesis_2 = [50, .01]

print("\n\tPart H:")

# For each test, we're calling the two tailed hypothesis method in hypothesis_test_functions.py
# We're inputting the test case to try and the data set that we're running it against
# I added a fancy print statement before that writes out our test case.
# Try to figure out how I concatenated the strings together!
test_1 = two_tailed_hypothesis_test(null_hypothesis_1, deliveries)
print("\nFor a mean of {mean} with alpha {alpha}:".format(mean=null_hypothesis_1[0],alpha=null_hypothesis_1[1]))
print(test_1)

test_2 = two_tailed_hypothesis_test(null_hypothesis_2, deliveries)
print("\nFor a mean of {mean} with alpha {alpha}:".format(mean=null_hypothesis_2[0],alpha=null_hypothesis_2[1]))
print(test_2)
