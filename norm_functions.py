from statistics import NormalDist # Available in Python 3.8+

# This function takes a data set and a list of conditions to check. It iterates through all of them automatically.
# From this function we want to return a set of outputs, in list form.
# The first entry in the list is another list! This is a header so we can remember the order in output!

def normal_distribution_probabilities(data, input):
    dist = NormalDist.from_samples(data) # We're defining the distribution as normal based upon inputted data set
    return_list = [["#", "< or >", "Probability"]]
    for pair in input: # This says to run this function for each pair in the list!
        if pair[1] == "less": # pair[1] references the second element in the pair, the more or less condition
            p = dist.cdf(pair[0]) # This calculates the probability in a cdf of the value being the input or less than
            pair.append(p) # This adds the probability calculated above into the list we iterated through, at the end
            return_list.append(pair) # This takes the modified list and appends it to the return list!
        elif pair[1] == "more": # The more method follows almost all the same logic, except it looks for more and has 1-p
            p = dist.cdf(pair[0])
            pair.append(1 - p)
            return_list.append(pair)
        else: # This is an error condition, it'll print out an error to the terminal if the input doesn't match either above
            print("input error!")
    return return_list