from statistics import NormalDist # Available in Python 3.8+

# Welcome to functions and how they work!
# Functions are repeatable blocks of code that serve a specific purpose, with both inputs and outputs
# As an example, here's a simple addition function (which python can just do with the + operator)

# def tells Python that we're declaring a function
# addition(): is the syntax that comes after a declaration, with the name of the function and (): after
# inputs go in (), in this example, we're inputting two values into the function
# the return line is what the function returns. In this case, we want to return a + b!
# Functions can have much more logic in the body, as evidenced below by the statistics ones!

def addition(value_1, value_2):
    return value_1 + value_2

# This function is how we calculate the z score, based upon a confidence level that is inputted
# the NormalDist() declaration is grabbing a normal distribution and initializing one (mean 0, std 1)
# running the .inv_cdf() method is the inverted continuous distrubution function, where we want to find the value, Z
# inside the .inv_cdf() method, we insert the confidence level with an adjustment for 2 tails
def z_score(confidence):
    return NormalDist().inv_cdf((1 + confidence) / 2.)

# This function calculates the confidence interval for a given data set that is inputted
# Setting confidence to 0.95 in the declaration gives it a default, so we only input confidence to set another one
def confidence_interval_exp(data, confidence=0.95):
  dist = NormalDist.from_samples(data) # This creates a normal distribution from a sample data set that is passed into function
  z = z_score(confidence) # This finds the z score from the z_score() method declared above
  h = dist.stdev * z # This creates the amount for the spread, from the distribution's std and the exact z score
  return dist.mean - h, dist.mean + h # This returns a list with the upper and lower bounds!

# The following function for the confidence interval of means is also very similar
# The main difference is in calculating h, where we divide z by square root of n
# len(data) automatically calculates the length of the list!
def confidence_interval_means(data, confidence=0.95, ddof=0):
  dist = NormalDist.from_samples(data)
  z = z_score(confidence)
  h = dist.stdev * z / ((len(data) - ddof) ** .5)
  return dist.mean - h, dist.mean + h